# Kuramoto

Simon + Assaf = ♡ 

## GPU optim

Various implementations

```
function f1(dx, x, p, t)
    dx .= sum(sin.((x .- transpose(x)) .* A), dims=2)
end
```
```
function f2(dx, x, p, t)
    mul!(dx, A, cos.(x))
    dx .= sin.(x) .* dx
end
```
```
function f3(dx, x, p, t)
    @tullio dx[i] := sin((x[i] - x[j])*A[i,j])
```
```
function f4(dx, x, p, t) #uses adjacency matrix
    @einsum dx[i] = sin(x[i] - x[L[i,j]])
end
```

Benchmark: for the same `A=rrg(n)` and `x=rand(n), dx=rand(n)` with $n=10000$, benchmarking `f(dx, x, nothing, nothing)`:  
|    data type   | `f1` | `f2` | `f3` | `f4` 
| ----------- | ----- | ---- | ---- | ----
| Array      | 618ms  | 273μs  |  857ms | -
| SparseArray   | 853 ms | 103ms   | 705ms  | -
Adj list | - | - | - | 259μs
CuArray | 682μs/9ms | **26μs**/2.5ms | | 796ms/790ms
CuSparseArray | 723μs/9ms | na


Last line : with or without `CUDA.@sync`.