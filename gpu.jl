using DifferentialEquations, CUDA, BenchmarkTools, LinearAlgebra, Graphs, Tullio, CUDAKernels, KernelAbstractions

function rrg(n)
    random_regular_graph(n,3)
end

function create_funcs(g)
    A = adjacency_matrix(g)
    B = Array(A)
    L = reduce(hcat, Graphs.SimpleGraphs.adj(g))
    G = cu(A)
    LG = cu(L)

    function f1(dx, x)
        dx .= sum(sin.((x .- transpose(x)) .* A), dims=2)
    end

    function f1_s(dx, x)
        dx .= sum(sin.((x .- transpose(x)) .* B), dims=2)
    end
    
    function f1_g(dx, x)
        dx .= sum(sin.((x .- transpose(x)) .* G), dims=2)
    end

    function f2(dx, x)
        mul!(dx, A, cos.(x))
        dx .= sin.(x) .* dx
    end

    function f2_s(dx, x)
        mul!(dx, B, cos.(x))
        dx .= sin.(x) .* dx
    end

    function f2_g(dx, x)
        mul!(dx, G, cos.(x))
        dx .= sin.(x) .* dx
    end

    function f3(dx, x)
        @tullio dx[i] := sin((x[i] - x[j])*A[i,j])
    end

    function f3_s(dx, x)
        @tullio dx[i] := sin((x[i] - x[j])*B[i,j])
    end


    function f4(dx, x)
        @tullio dx[i] := sin(x[i] - x[L[j,i]])
    end
 
    function f4_g(dx, x)
        @tullio dx[i] := sin(x[i] - x[L[j,i]])
    end   

    return [f1, f1_s, f2, f2_s, f3, f3_s, f4],[f1_g, f2_g, f4_g]
end

function test(init, f)
    prob = ODEProblem(f, init, (0.0f0, 10.0f0))
    solve(prob)
end

function test_cpu(n)
    @info "Testing CPU"
    A, dx, x = rrg(n), rand(n), rand(n)
    f = create_funcs(A)
    @btime $f($dx, $x, nothing, nothing)
end

function test_gpu(n)
    @info "Testing GPU"
    A, dx, x = cu(rrg(n)), cu(rand(n)), cu(rand(n))
    f = create_funcs(A)
    @btime CUDA.@sync $f($dx, $x, nothing, nothing)
end

function test_ode_cpu(n)
    @info "Testing ODE on CPU"
    A = rrg(n)
end

N = 10000
dx,x = rand(N),rand(N)
#test_cpu(N)
#test_gpu(N)


