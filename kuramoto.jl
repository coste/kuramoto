using DifferentialEquations, Graphs, LambertW, ProgressBars, CUDA
using BSON:@save
include("logger.jl")



ϱ(x,k) = sum(exp.(k*1im .* x)) / size(x)[1]
ζ(c::Real) = 1 + lambertw(-c*exp(-c))/c #relative size of largest component



function extract_giant_component(g::SimpleGraph)::SimpleGraph
    vertices_in_giant = sort(connected_components(g), by=size)[end]
    induced_subgraph(g, vertices_in_giant)[1]
end

function extract_core_graph(g::SimpleGraph, k::Int)::SimpleGraph
    vertices_in_kcore = k_core(g, k)
    induced_subgraph(g, vertices_in_kcore)[1]
end

const er = erdos_renyi
const rrg = random_regular_graph
er_core(n,d,k=2) = extract_core_graph(er(n,d/n), k)
er_giant(n,d) = extract_giant_component(er(n,d/n))


function solve_kuramoto_on(
    graph::SimpleGraph; 
    T=10., 
    solver=BS3(), 
    reltol=1e-8, 
    abstol=1e-8, 
    type = Float32)

    n, L = nv(graph), Graphs.SimpleGraphs.adj(graph)
    init = convert(Vector{type}, 2*pi .* rand(n))

    function dV!(dV,θ,p,t)
        dV .= - [sum([sin(θ[i] - θ[j]) for j in L[i]]) for i in 1:n]
    end

    ode = ODEProblem(dV!, init, (0.0, T)) 
    return solve(ode, solver, reltol=reltol, abstol=abstol)
end


function solve_kuramoto_gpu(
    graph::SimpleGraph; 
    T=10.0f0, 
    solver=BS3(), 
    reltol=1e-8, 
    abstol=1e-8)

    n, A = nv(graph), cu(adjacency_matrix(graph))
    init = cu(2*pi .* rand(n))

    function f!(du, u, p, t)
        - sum(sin.(u .- transpose(u)) .* A, dims=2)
    end

    ode = ODEProblem(f!, init, (0.0f0, T)) 
    return solve(ode, solver, reltol=reltol, abstol=abstol)
end



function experiment(n::Int, T::Real, degree_range, N, N_eval, use_gpu::Bool)
    
    ret = Dict()
    eval_times = LinRange(0,T,N_eval)
    for d in ProgressBar(degree_range)
        _results = zeros(N, N_eval)
        for i in 1:N
            g = er_giant(n,d)
            sol = solve_kuramoto_on(g; T=T)
            _results[i,:] = [abs(ϱ(sol(t),1)) for t in eval_times]
        end
        ret[d] = _results
    end
    return ret
end

function experiment(n=100,T=10.; dmin=2., dmax=3., eps=0.1, N=8, N_eval=10, log=true, use_gpu = false)

    degree_list = dmin:eps:dmax
    result = experiment(n, T, degree_list, N, N_eval, use_gpu)

    if log
        fname = LOGGER(;n=n,T=T,dmin=dmin,dmax=dmax,N=N)
        @info "Saving to $fname"
        log2file(fname; 
            degree_list = degree_list, 
            eval_times=LinRange(0,T,N_eval),
            result = result)
    end

    result
end




#solve_kuramoto_on(erdos_renyi(1000, 3.0/1000))



function plot_histogram(x, nbins=100)
    histogram(mod2pi.(x), nbins=nbins)
    xlims!(0,2*pi)
end
