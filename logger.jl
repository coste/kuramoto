# Simon's logging utility

mutable struct filename_logger
    filetype
    folder
    base
end

function (F::filename_logger)(;kwargs...)
    content = prod(["_" * string(key) * string(val) for (key, val) in Dict(kwargs)])
    F.folder * F.base * content * "." * F.filetype
end

function log2file(fname;kwargs...)
    D = Dict(kwargs)
    @save(fname, D)
end

const LOGGER = filename_logger("bson", "save/log/", "exp")
