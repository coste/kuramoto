using Graphs, BenchmarkTools
include("kuramoto.jl")

function test_allocs(n=1000,d=10)
    """
    Reducing allocs for the ODE func
    """
    g = erdos_renyi(n, d/n)
    A = adjacency_matrix(g)
    L = Graphs.SimpleGraphs.adj(g)

    f1(x) = - sum(sin.(x .- transpose(x)) .* A, dims=2)
    f2(x) = -[sum([sin(x[i] - x[j]) for j in L[i]]) for i in 1:n]

    loc_energy(t,v) = sum(sin.(t .- v))

    f3(x) = [loc_energy(x[i], x[L[i]]) for i in 1:n]
    f4(x) = loc_energy.(x, getindex.(Ref(x), L))
    
    x = rand(n)
    @btime $f1(x)
    @btime $f2(x)
    @btime $f3(x)
    @btime $f4(x)
end

function test_type(n=1000,d=5)
    g = er(n,d/n)
    @btime solve_kuramoto_on($g; type=Float64)
    @btime solve_kuramoto_on($g; type=Float32)
    #@btime solve_kuramoto_on($g; type=Float16) #=> get NaN
end

function test_cuda(n=10000,d=5)
    g = er(n,d/n)
    @btime solve_kuramoto_on($g)
    @btime solve_kuramoto_gpu($g)
end

#test_type()
test_cuda()
