using Graphs

function is_arboricol(d)
    sum(d) == 2*length(d)
end

function random_uniform_tree(
    n::Integer,
    rng::Union{Nothing,AbstractRNG}=nothing,
    seed::Union{Nothing,Integer}=nothing,)

    n==1 && return Graph(1,0)
    n==2 && return Graph(2,1)
    rng = rng_from_rng_or_seed(rng, seed)
    code = rand(rng, 1:n, n-2)
    return decode(code)
end

function random_configuration_tree(
    d)
    isgraphical(d)
end